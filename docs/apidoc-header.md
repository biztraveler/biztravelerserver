## API URL prefixes

All authentication URLs are the full path -- e.g, you should redirect your user to `https://example.com/auth/linkedin/init` if the docs say `/auth/linkedin/init`. However, all other requests must be prefixed with `/api/v1`. For example, `/tripit-sync` should actually be sent to `https://example.com/api/v1/tripit-sync`.

## Login process explained

The backend should redirect the user to the "init" authentication endpoint for the correct service (linkedin or tripit). The redirect URLs in the authentication documentation tell where the user will be redirected to once the authentication on the 3rd party site is complete. For example, after loggin in with linkedin, the user will go back to `https://frontend.com/callback-linkedin-ok`.

## Errors

On error, the return value will be in the following format:

```json
{
    "status": "failure",
    "errorMessage": "geocode_failed"
}
```

That is what would happen if the error message were `geocode_failed`. On success, there will always be a `"status": "success"` in the returned JSON in addition to other properties.
