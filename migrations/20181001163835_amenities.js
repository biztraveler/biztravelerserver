'use strict';

exports.up = async knex => {
    await knex.schema.createTable('amenity', table => {
        table.increments();
        table.timestamps(true, true);

        table.integer('trip_id').notNull().references('trip.id');
        table.string('tripit_id').unique();

        table.string('name').notNull(); // supplier_name in tripit
        // not using an enum because knex doesn't support adding new values in a migration very easily
        table.string('type').notNull();
        table.specificType('point', 'geometry(POINT, 4326)').notNull();
    });
};

exports.down = async knex => {
    await knex.schema.dropTable('amenity');
};
