
exports.up = async (knex, Promise) => {
    await knex.schema.createTable('user', table => {
        table.increments('id');
        table.string('given_name').notNullable();
        table.string('family_name');
        table.string('email').notNullable();
        table.string('image');
        table.timestamps();
    });
    await knex.schema.createTable('community', table => {
        table.increments('id');
        table.string('name').notNullable();
        table.string('description');
        table.string('image');
        table.timestamps();
    });
    await knex.schema.createTable('user_community', table => {
        table.integer('user_id');
        table.integer('community_id');
        table.primary(['user_id', 'community_id']);
        table.foreign('user_id').references('id').inTable('user');
        table.foreign('community_id').references('id').inTable('community');
    });
}

exports.down = async (knex, Promise) => {
    await knex.schema.dropTable('user_community');
    await knex.schema.dropTable('user');
    await knex.schema.dropTable('community');
}
