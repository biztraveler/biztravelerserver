
exports.up = async knex => {
    await knex.schema.table('user', table => {
        table.string('linkedin_id').unique().notNullable();
        table.string('tripit_id').unique();
        table.string('headline').notNullable();
        table.string('position');
        table.string('human_location').notNullable();
        // we forgot to pass true as arguments to timestamps.
        // by default, it uses datetime, and no default.
        // it is better to use timestamp and default to the present
        // in theory, we should save the timestamps and then
        // re-insert them, but this isn't production!
        table.dropTimestamps();
    });
    await knex.schema.table('user', table => {
        table.timestamps(true, true);
    });
};

exports.down = async knex => {
    await knex.schema.table('user', table => {
        table.dropColumn('linkedin_id');
        table.dropColumn('tripit_id');
        table.dropTimestamps();
    });
    await knex.schema.table('user', table => {
        table.timestamps();
    });
};
