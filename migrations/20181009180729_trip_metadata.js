'use strict';

exports.up = async knex => {
    await knex.schema.table('trip', table => {
        table.string('description');
        table.string('image');
    });
};

exports.down = async knex => {
    await knex.schema.table('trip', table => {
        table.dropColumn('description');
        table.dropColumn('image');
    });
};
