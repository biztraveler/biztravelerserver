
exports.up = async knex => {
    await knex.schema.table('user', table => {
        table.string('tripit_access_key');
        table.string('tripit_access_secret');
    });
};

exports.down = async knex => {
    await knex.schema.table('user', table => {
        table.dropColumn('tripit_access_key');
        table.dropColumn('tripit_access_secret');
    });
};
