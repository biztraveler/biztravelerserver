exports.up = async knex => {
    await knex.schema.createTable('trip', table => {
        table.increments('id');
        table.timestamps(true, true);

        table.integer('user_id').references('user.id').notNull();
        table.string('tripit_id').unique();
        table.date('started_at').notNull();
        // maybe in-progress trips?
        table.date('ended_at');
        // machine readable
        table.specificType('visited_point', 'geometry(POINT, 4326)').notNull();
        // human readable   
        table.string('visited_name');
    });
};

exports.down = async knex => {
    await knex.schema.dropTable('trip');
};
