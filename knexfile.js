const config = require('./src/lib/config');

module.exports = {
    development: {
        client: 'pg',
        connection: config.connectionString,
    },
};
