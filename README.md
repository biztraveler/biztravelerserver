# Biztraveler: Backend

### Getting Started

- Biztraveler is a full-stack web application built with React on the front end and a Node.js API as a backend with  a PostgreSQL Database.  

#### Prerequisites

- Before you begin you need a number of prerequisites including... 
    - PostgreSQL, installed and running
    - Node.js and npm installed (at least Node 8, earlier versions do not support async/await syntax)
    - Linkedin API Client ID and secret [Linkedin Developer Page](https://linkedin.com/developer/apps/new)
    - Tripit API ID and secret [Tripit Developer Page](https://www.tripit.com/developer)
    - [Biztraveler Front End](https://bitbucket.org/biztraveler/biztravelerapp/src/master/) Installed and running...
    - Go to the directory where you normally put development projects, and create a parent directory to store both the front and backend code repositories.
    - Then clone the front end by running `npm install` and then `npm run watch` in order to view a hot reloading development version of the front end with [Webpack Dev Server](https://webpack.js.org/configuration/dev-server/)

```
git clone git@bitbucket.com:biztraveler/biztravelerapp
cd biztravelerapp
npm install
npm run watch
```

- Now, navigate to http://localhost:8080/ in order to view your working front end.
    - NOTE:  `npm run build` will bundle the entire front end   with [webpack](https://webpack.js.org/) in preparation for deployment.
 

### Installing the Backend
- Navigate back to your newly created parent directory.
- Then, clone the backend repository `git clone git@bitbucket.com:biztraveler/biztravelerserver`
- Then run `npm install` from the biztravelerserver directory to install all project dependencies.

### Setting environment variables

The server and DB migration scripts all load environment variables from the `.env` file. There is a `.env.example` file included in the repository with examples for all possible environment variable options. Copy the examples out of the file: `cp .env.example .env` then create a new .env file `.env` with your api keys as required.

- Important!  The `BIZ_CONNECTION_STRING` variables are critical for connecting to the local Postgres Database.

#### Example .env file...

```
# To use this file, copy it to ".env", and uncomment relevant lines

# Database connection string
# Example for Linux/Mac with Unix sockets:
#BIZ_CONNECTION_STRING=postgresql://localhost/biztraveler
# Example for Windows over TCP (default):
#BIZ_CONNECTION_STRING=postgresql://localhost:5432/biztraveler

# Which port to listen on
# If you change this, don't forget to change BIZ_BASE_URL too!
# localhost:8170 (default):
#BIZ_HTTP_PORT=8170

# Linkedin API Credentials
#BIZ_LINKEDIN_ID=12345abcdef
#BIZ_LINKEDIN_SECRET=12345abcdef

# Tripit API Credentials
# The "ID" should actually be your "key". I just want to use the same terms for both linkedin and here.
# That being said, "key" is actually the correct term in oauth language.
#BIZ_TRIPIT_ID=12345abcdef
#BIZ_TRIPIT_SECRET=12345abcdef

# Cookie secret
# Set it to something random, just make up some gibberish words. You do not need to memorize it.
#BIZ_COOKIE_SECRET=12345abcdef

# Domain for the cookie. Leave empty to use the backend domain.
#BIZ_COOKIE_DOMAIN=biztraveler.com

# The base URL (protocol, hostname, and port) where the backend is being served
#BIZ_BASE_URL=http://localhost:8170

# Where the frontend server is, same format as for main BIZ_BASE_URL
#BIZ_FRONTEND_BASE_URL=http://localhost:3000
```

### Setting up the database

Install PostgreSQL on your system, then create a database for biztraveler. If you are on Linux or macOS, you should switch to the `postgres` user (with `su postgres`), then run `createdb -O your_username biztraveler`, replacing your_username. On Windows, things will be slightly different.

Make sure to set `BIZ_CONNECTION_STRING` in `.env` to the correct connection string on your database. On linux and macOS, you can use `postgresql://localhost/biztraveler` to use a Unix socket.

Once you've finished with `.env`, run `knex migrate:latest` to bring the database schema up-to-date.

### Detailed Database Instructions (Mac Specific)

If you are new to Postgres or if you do not have it install on your machine follow the instructions below.

- First verify that you have postgres installed locally in your terminal by running **postgres --version**

```
User-MBP:biztravelerserver User$ postgres --version
   postgres (PostgreSQL) 10.3
```

- (Optional) If you like to have a GUI interface you can download and install Postgres.app in your applications folder at (https://postgresapp.com/)

- Next, in your terminal, create a database **createdb -O username biztravelerserver**
    - Your terminal should then open a postgres shell that will start with a prompt like so **user#=**

```
User-MBP:biztravelerserver User$ createdb -O michaelcastor biztravelerserver
User-MBP:biztravelerserver User$ "/Applications/Postgres.app/Contents/Versions/10/bin/psql" -p5433 -d "username"
psql (10.5)
Type "help" for help.
```

- Inside the Postgres shell, create a database called biztraveler **create database biztraveler**

```
user=# create database biztraveler;
CREATE DATABASE

```

- Verify that you are logged in using the correct user name **\c biztraveler**

```
user=# \c biztraveler
You are now connected to database "biztraveler" as user "username".
```

- Now, you will also need to add an extension for postgis **create extension postgis;**

```
biztraveler=# create extension postgis;
CREATE EXTENSION
```

- You can verify success by viewing the schema.  In the postgres shell, run **/d** (at this point you should only have the postgis schema)
```
biztraveler=# \d
                 List of relations
 Schema |       Name        | Type  |     Owner     
--------+-------------------+-------+---------------
 public | geography_columns | view  | username
 public | geometry_columns  | view  | username
 public | raster_columns    | view  | username
 public | raster_overviews  | view  | username
 public | spatial_ref_sys   | table | username
(5 rows)
```

### Database migrations. 
 - Now that you have your database set up and the BIZ_CONNECTION_STRING in .env and you've run `CREATE EXTENSION postgis`, you are ready to migrate the test database.  

- Install [knex](https://knexjs.org/) globally (`npm install --global knex` or npm i -g knex)
    
- Run migrations (`knex migrate:latest`)



### Running the server

- Now that you have the database setup locally you can run `node index.js` to run the server. Go to localhost:8170 to view it.

```
User-MBP:biztravelerserver User$ node index.js
BIZTRAVELER server UP
```
![localhost:8170](./docs/images/localhost:8170.png) 

### Authorizing External API's

Now that you have the database installed and the server running you can follow the steps  below to authorize the Linkedin and Tripit API's to work with the BizTraveler Server Application

- Navigate to **localhost:8170/auth/linkedin/init**
- Upon successful authorization from **Linkedin** you will be redirected to the front end of the application at **http://localhost:8080/callback-linkedin-ok**

![localhost:8080/callback-linkedin-ok](./docs/images/localhost:8080-callback-linkedin-ok.png) 

- Next, navigate to **localhost:8170/auth/tripit/init**
- You will be redirected to login with your tripit basic auth credentials.
- Upon successful authoriztion from **tripit** you will be once again redirected to the front end of the application at **http://localhost:8080/callback-tripit-ok**

- You can verify that your profile information was uploaded correctly by navigating to **http://localhost:8170/api/v1/user/me**

```
	{
	• status: "success",
	• profile: 
{
	• id: 2,
	• givenName: "Timmy",
	• familyName: "Traveler",
	• email: "ilovetotravel@gmail.com",
	• image: "https://media.licdn.com/dms/image/C5603AQGYYpR6Dt-95g/profile-displayphoto-shrink_100_100/0?e=1543449600&v=beta&t=mfeJNGGZMcn1MCgV2Q9NU1e4kzjZ-pVRxLvlJut3WBA",
	• headline: "Software Developer",
	• position: "Freelance Software Developer at BizTravler",
	• humanLocation: "Greater Seattle Area",
	• createdAt: "2018-09-26T02:50:18.037Z",
	• tripitLinked: true
}
	}
``` 

- 	Access an array of your tripit trips data by navigating to **http://localhost:8170/api/v1/trip/list**

```
	{
	• status: "success",
	• trips: 
[
	• {
	• id: 1,
	• userId: 2,
	• tripitId: "192586096",
	• startedAt: "2017-06-04T07:00:00.000Z",
	• endedAt: "2017-06-08T07:00:00.000Z",
	• visitedPoint: 
{
	• lon: -122.14302,
	• lat: 37.44188
},
	• visitedName: "Palo Alto, CA"
},
	• {
	• id: 2,
	• userId: 2,
	• tripitId: "190273799",
	• startedAt: "2017-02-19T08:00:00.000Z",
	• endedAt: "2017-02-21T08:00:00.000Z",
	• visitedPoint: 
{
	• lon: -122.04024,
	• lat: 37.52966
},
	• visitedName: "Newark, CA"
}
]
}
```

 