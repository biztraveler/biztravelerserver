'use strict';

/**
 * This isn't the best place to put this stuff, is it?
 * 
 * @apiDefine 404 Not found 404
 */
/**
 * @apiDefine 403 Forbidden 403
 */

// provides utility functions to wrap a response in success or failure
module.exports = (req, res, next) => {
    // utility functions
    res.sendSuccess = json => {
        res.status(200).send({
            status: 'success',
            ...json,
        });
    };
    res.sendFailure = (code, reason) => {
        res.status(code).send({
            status: 'failure',
            errorMessage: reason,
        });
    }
    // authentication
    if (!req.user) {
        return res.sendFailure(401, 'Not logged in.');
    }
    // all clear
    next();
};
