'use strict';

const assert = require('chai').assert;
const debug = require('debug')('biz.tripit');
const tripitApi = require('./tripit-api');
const tripitDb = require('./trip-db');
const amenity = require('./amenity');

const tripitAll = {
    // pings the tripit API to update user trips.
    // TODO: remove old trips that were deleted on tripit
    updateTrips: async user => {
        assert.isObject(user);
        debug('Fetching trips from tripit...');
        const tripit = await tripitApi.getPastTrips(user);
        debug(`Fetched ${tripit.trips.length} trips and ${tripit.amenities.length} amenities.`);
        for (const tripitTrip of tripit.trips) {
            await tripitDb.upsertFromTripit(tripitTrip);
        }
        debug('All trips inserted to DB.');
        for (const tripitAmenity of tripit.amenities) {
            debug('Amenity:');
            debug(tripitAmenity);
            await amenity.upsertByTripit(tripitAmenity);
        }
    }
};
module.exports = tripitAll;
