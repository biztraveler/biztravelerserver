'use strict';

const debug = require('debug')('biz.tripit-api');
const assert = require('chai').assert;
const request = require('request-promise-native');
const xml2jsCb = require('xml2js');
const pify = require('pify');
const convert = require('../lib/convert');
const config = require('../lib/config');

/**
 * Trip object:
 * 
 */

const xml2js = pify(xml2jsCb.parseString);

/**
 * @param user a User object
 * @param url the portion of the URL after /api/v1 to GET. Include leading slash.
 * @return the parsed JSON response
 */
const makeAuthenticatedRequest = async (user, url) => {
    assert.property(user, 'tripitAccessKey');
    assert.property(user, 'tripitAccessSecret');
    const xmlRes = await request.get({
        url: config.tripitApiBaseUrl + url,
        oauth: {
            consumer_key: config.tripitId,
            consumer_secret: config.tripitSecret,
            token: user.tripitAccessKey,
            token_secret: user.tripitAccessSecret,
        },
    });
    // this is not in convert.js because it is weird/pseudo-asynchronous and also works on a string
    const jsonRes = await xml2js(xmlRes);
    debug(`Response to tripit: ${url}`);
    debug(jsonRes);
    return jsonRes;
};

const tripitApi = {
    /**
     * @param user a User object, to fetch the trips for
     * @return an array of Trips
     */
    getPastTrips: async user => {
        const tripitTrips = await makeAuthenticatedRequest(user, '/list/trip/past/true/include_objects/true');
        const publicTrips = convert.tripit2PublicTrips(tripitTrips, user.id);
        // the publicity of amenities is determined later.
        const amenities = convert.tripit2Amenities(tripitTrips);
        return {
            trips: publicTrips,
            amenities,
        };
    },
};
module.exports = tripitApi;
