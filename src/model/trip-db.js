'use strict';

const debug = require('debug')('biz.trip-model');
const assert = require('chai').assert;
const R = require('ramda');

const db = require('../lib/db');
const dbUtil = require('../lib/db-util');
const convert = require('../lib/convert');
const config = require('../lib/config');
const knexFragments = require('./knex-fragments');

const tripitDb = {
    /**
     * updates or creates a trip given a converted trip from tripit
     * @param trip a trip object
     * @return the inserted trip, including id
     */
    upsertFromTripit: async trip => {
        assert.isObject(trip);
        assert.notProperty(trip, 'id');
        assert.property(trip, 'tripitId');
        const dbTrip = await dbUtil.upsert('trip', 'tripit_id', convert.trip2Db(trip));
        //return convert.db2Trip(dbTrip);
    },
    /**
     * Find all trips for a user.
     * @param user a user object
     * @return a list of trips for that user.
     */
    findByUser: async user => {
        assert.property(user, 'id');
        const dbTrips = await knexFragments.tripBaseSelect().where('user_id', user.id);
        const jsTrips = dbTrips.map(convert.db2Trip);
        return jsTrips;
    },
    findOneByTripitId: async tripitId => {
        // not using dbUtil.findOne because of the postgis point
        const dbTrip = await knexFragments.tripBaseSelect().where('tripit_id', tripitId);
        if (dbTrip.length > 0) {
            return convert.db2Trip(dbTrip[0]);
        } else {
            return false;
        }
    },
    findOneById: async tripId => {
        const dbTrip = await knexFragments.tripBaseSelect().where('id', tripitId);
        if (dbTrip.length > 0) {
            return convert.db2Trip(dbTrip[0]);
        } else {
            return false;
        }
    },
    /**
     * @param point the point to find trips near
     * @param radius how far away to find trips, in meters
     * @param maxTrips maximum number of trips to return, will be internally clamped
     */
    findNearby: async (geocoded, maxTripsArg = Infinity) => {
        assert.property(geocoded, 'geoJson');
        assert.isNumber(maxTripsArg);
        const maxTrips = R.clamp(1, 100, maxTripsArg);
        const dbTripsQuery = knexFragments.tripWhereWithin(
            config.maxDistanceTolerance,
            geocoded.geoJson,
            knexFragments.tripBaseSelect(),
        ).orderByRaw('RANDOM()').limit(maxTrips);
        const dbTrips = await dbTripsQuery;
        debug('Number of trips returned:');
        debug(dbTrips.length);
        const jsTrips = dbTrips.map(c => convert.db2Trip(c));
        return jsTrips;
    },
    /**
     * @param tripId the trip Id to update
     * @param metadata a trip object containing only, at most, description and image
     * @return whether the update was successful
     */
    updateMetadataById: async (tripId, metadata) => {
        // prevent modifying other data
        const sanitizedMetadata = R.pick([ 'description', 'image' ], metadata);
        if (R.isEmpty(sanitizedMetadata)) {
            return true;
        }
        const moddedRows = await db('trip').update(sanitizedMetadata).where('id', tripId);
        assert.isBelow(moddedRows, 2);
        return moddedRows === 1;
    },
};
module.exports = tripitDb;
