'use strict';

const debug = require('debug')('biz.geocode');
const request = require('request-promise-native');
const convert = require('../lib/convert');
const config = require('../lib/config');

const geocode = {
    forward: async human => {
        const escapedHuman = encodeURIComponent(human);
        const nominatimTextRes = await request({
            url: config.nominatimSearchUrl + escapedHuman,
            qs: {
                format: 'json',
                dedupe: 1,
                limit: 1,
                polygon_geojson: 1,
            },
            headers: {
                'User-Agent': config.nominatimUserAgent,
            },
        });
        debug('Geocoded length:');
        debug(nominatimTextRes.length);
        try {
            const nominatimRes = JSON.parse(nominatimTextRes);
            if (nominatimRes.length < 1) {
                return false;
            }
            return convert.nominatim2Geocoded(nominatimRes);
        } catch (e) {
            // let's hope it was the JSON parsing
            return false;
        }
    },
};
module.exports = geocode;
