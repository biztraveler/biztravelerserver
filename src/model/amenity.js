'use strict';

const debug = require('debug')('biz.amenity');
const assert = require('chai').assert;

const trip = require('./trip-db');
const db = require('../lib/db');
const dbUtil = require('../lib/db-util');
const convert = require('../lib/convert');
const knexFragments = require('./knex-fragments');

const amenityModel = {
    /**
     * @param amenity without id or tripId
     * @return whether it was inserted or not
     * This will determine the tripId based on tripitTripId.
     */
    upsertByTripit: async amenity => {
        assert.property(amenity, 'tripitId');
        assert.property(amenity, 'tripitTripId');
        assert.notProperty(amenity, 'tripId');

        // Find the corresponding Trip ID
        const myTrip = await trip.findOneByTripitId(amenity.tripitTripId);
        // if there is no trip, it might be private or something -- don't add
        if (!myTrip) {
            return false;
        }
        const tripId = myTrip.id;

        const fullAmenity = {
            ...amenity,
            tripId,
        };
        debug('About to upsert amenity:');
        debug(fullAmenity);
        await dbUtil.upsert('amenity', 'tripit_id', convert.amenity2Db(fullAmenity));
        return true;
    },

    /**
     * @param trip a trip
     * @return an array of amenities for that trip.
     */
    findByTrip: async myTrip => {
        assert.property(myTrip, 'id');

        const dbAmenities = await knexFragments.amenityBaseSelect().where('trip_id', myTrip.id);
        return dbAmenities.map(convert.db2Amenity);
    }
};
module.exports = amenityModel;
