// here lie the knex helper functions
'use strict';

const db = require('../lib/db');

const knexFragments = {
    tripBaseSelect: () => db('trip').select('*', db.postgis.asGeoJSON('visited_point')),
    amenityBaseSelect: () => db('amenity').select('*', db.postgis.asGeoJSON('point')),
    /**
     * @param maxDistance how far away from the geoJSON the trip may be, in meters
     * @param geoJson the geoJSON, but not in JSON, just a JS object
     * @param fragment a partially constructed knex query which includes the trip table
     * @return fragment but with an additional where clause
     */
    tripWhereWithin: (maxDistance, geoJson, fragment) =>
        fragment.where(db.raw('ST_DWithin(??, ?::geography, ?)',
            [
                'trip.visited_point',
                db.postgis.setSRID(db.postgis.geomFromGeoJSON(geoJson), 4326),
                maxDistance,
            ],
        )),
};
module.exports = knexFragments;
