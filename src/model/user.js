'use strict';

const assert = require('chai').assert;
const debug = require('debug')('biz.user-model');
const R = require('ramda');

const convert = require('../lib/convert');
const db = require('../lib/db');
const dbUtil = require('../lib/db-util');
const config = require('../lib/config');
const knexFragments = require('./knex-fragments');

const user = {
    /**
     * @param id user id (our own, not linkedin or tripit)
     * @return the user object, if it exists.
     */
    findOneById: async id => {
        assert.isNumber(id);
        const dbUser = await dbUtil.findOne('user', 'id', id);
        return dbUser ? convert.db2User(dbUser) : false;
    },
    // @param user = a User object to create the account from, without tripit
    // @return = undefined
    // will throw an error if there is a unique constraint violation
    upsert: async user => {
        assert.isObject(user);
        // if it has an id, it will conflict with things for sure
        assert.notProperty(user, 'id');

        const dbUser = await dbUtil.upsert('user', 'linkedin_id', convert.user2Db(user));
        return convert.db2User(dbUser);
    },
    /**
     * @param user the user account to modify
     * @param tripitId the tripit ID
     * @param accessKey the tripit access key
     * @param accessSecret the tripit api secret
     * @return void
     */
    linkTripit: async (user, tripitId, accessKey, accessSecret) => {
        assert.isNumber(user.id);
        [ tripitId, accessKey, accessSecret ].forEach(c => assert.isString(c));

        const dbUser = convert.user2Db({
            tripitId,
            tripitAccessKey: accessKey,
            tripitAccessSecret: accessSecret,
        });
        await db('user').update(dbUser).where('id', user.id);
    },
    /**
     * @param geocoded a geocode object
     * @param maxUsers the maximum number of limits to return, will be internally clamped
     * @return an array of users
     */
    findExperts: async (geocoded, maxUsersArg = Infinity) => {
        const maxUsers = R.clamp(1, 100, maxUsersArg);
        const dbUsers = await knexFragments.tripWhereWithin(
                config.maxDistanceTolerance,
                geocoded.geoJson,
                db('user').select('user.*')
                    .innerJoin('trip', 'trip.user_id', 'user.id')
            )
            .groupBy('user.id')
            // I'm not sure if this can be done without knex.raw, but i certainly don't know how.
            .orderBy(db.raw('count("trip".*)'))
            .having(db.raw('count("trip".*)'), '>=', config.minExpertTrips)
            .limit(maxUsers);
        debug(`Experts found from DB: ${dbUsers.length}`);
        const jsUsers = dbUsers.map(convert.db2User);
        return jsUsers;
    },
};
module.exports = user;
