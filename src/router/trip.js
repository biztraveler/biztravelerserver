'use strict';

const debug = require('debug')('biz.trip-router');
const express = require('express');
const pMap = require('p-map');
const R = require('ramda');
const Boom = require('boom');

const convert = require('../lib/convert');
const apiWrap = require('../lib/api-wrap');
const tripit = require('../model/tripit-all');
const tripModel = require('../model/trip-db');
const user = require('../model/user');
const amenity = require('../model/amenity');
const geocode = require('../model/geocode');

const trip = new express.Router();

/**
 * @api {post} /tripit-sync Re-sync trips from Tripit
 * @apiName tripitSync
 * @apiGroup trip
 * 
 * @apiSuccess success The trip synced successfully
 */
trip.get('/tripit-sync', apiWrap(async (req, res) => {
    await tripit.updateTrips(req.user);
    res.sendSuccess();
}));

/**
 * @api {patch} /trip/:id Update the description and image of a trip.
 * @apiName updateTripMetadata
 * @apiGroup trip
 * 
 * @apiParam {number} id the internal trip ID (not tripit ID)
 * @apiParam {string} description the new trip description
 * @apiParam {string{..255}} image url to an image for the trip
 * 
 * @apiParamExample Example request:
 *     PATCH /trip/45
 *     { "description": "To LinuxFest NorthWest", "image": "https://google.com" }
 * 
 * @apiSuccess success the trip was updated
 * 
 * @apiError (404) trip_not_found the trip was not found
 * @apiError (403) wrong_user the trip belongs to a different user
 */
trip.patch('/trip/:id', apiWrap(async (req, res) => {
    const tripId = req.params.id;
    const trip = await tripModel.findOneById(tripId);
    if (trip.user_id !== req.user.id) {
        throw Boom.forbidden('wrong_user');
    }
    debug(`Updating trip ${tripId} with data:`);
    debug(req.body);
    if (await tripModel.updateMetadataById(tripId, req.body)) {
        return res.sendSuccess({});
    } else {
        throw Boom.notFound('trip_not_found');
    }
}));

/**
 * @api {get} /trip/nearby find trips, users, amenities, and experts near a point
 * @apiName searchNearby
 * @apiGroup trip
 * 
 * @apiParam {number} human_location a human-readable location which will be geocoded into lat/lon on the backend.
 * @apiParamExample human_location usage:
 *                  /trip/nearby?human_location=iceland
 * 
 * @apiSuccess {object} geocoded the results of the geocoding
 * @apiSuccess {object} geocoded.center the center of the searched-for location (lat/lon)
 * @apiSuccess {object} geocoded.viewport what should be displayed for the edges of the map
 * @apiSuccess {object} geocoded.viewport.northeast a lat/lon object
 * @apiSuccess {object} geocoded.viewport.southwest a lat/lon object
 * 
 * @apiSuccess {object[]} trips several trips selected at random from trips within the query area. See the /user/:id endpoint for documentation of the trip format.
 * @apiSuccess {object[]} users all of the user accounts related to the trips. See /user/:id endpoint for documentation of the user format.
 * @apiSuccess {object[]} experts all of the user accounts which are deemed to be "experts" in this location. Same format as users, see /user/:id for docs.
 * 
 * @apiSuccess {object[]} amenities a list of "amenities" related to the returned trips
 * @apiSuccess {number} amenities.id the internal ID of an amenity
 * @apiSuccess {number} amenities.tripId trip ID the amenity is related to
 * @apiSuccess {string{..255}} amenities.name human-readable name
 * @apiSuccess {string{..255}} amenities.type (eg, "restauant")
 * @apiSuccess {object} amenities.point the location of the amenity (has props lat/lon)
 * 
 * @apiSuccessExample {json} Successful call
 *        {
 *          "status": "success",
 *          "geocodeOk": true,
 *          "geocoded": {
 *            "center": {
 *              "lat": "64.9841821",
 *              "lon": "-18.1059013"
 *            },
 *            "viewport": {
 *              "northeast": {
 *                "lat": "63.0859177",
 *                "lon": "-25.0135069"
 *              },
 *              "southwest": {
 *                "lat": "67.353",
 *                "lon": "-12.8046162"
 *              }
 *            }
 *          },
 *          "trips": [
 *            {
 *              "id": 2,
 *              "userId": 1,
 *              "startedAt": "2018-06-03T07:00:00.000Z",
 *              "endedAt": "2018-06-04T07:00:00.000Z",
 *              "visitedPoint": {
 *                "lon": -21.89541,
 *                "lat": 64.13548
 *              },
 *              "visitedName": "Reykjavik, Iceland",
 *              "description": "hello world.",
 *              "image": "hello world."
 *            },
 *            {
 *              "id": 3,
 *              "userId": 1,
 *              "startedAt": "2018-07-09T07:00:00.000Z",
 *              "endedAt": "2018-07-14T07:00:00.000Z",
 *              "visitedPoint": {
 *                "lon": -22.0749,
 *                "lat": 64.32179
 *              },
 *              "visitedName": "Akranes, Iceland",
 *              "description": null,
 *              "image": null
 *            }
 *          ],
 *          "experts": [
 *            {
 *              "id": 1,
 *              "givenName": "Mark",
 *              "familyName": "Polyakov",
 *              "image": null,
 *              "headline": "hello",
 *              "position": "developer at BizTraveler",
 *              "humanLocation": "United States",
 *              "createdAt": "2018-09-26T02:42:01.163Z",
 *              "tripitLinked": true
 *            }
 *          ],
 *          "users": [
 *            {
 *              "id": 1,
 *              "givenName": "Mark",
 *              "familyName": "Polyakov",
 *              "image": null,
 *              "headline": "hello",
 *              "position": "developer at BizTraveler",
 *              "humanLocation": "United States",
 *              "createdAt": "2018-09-26T02:42:01.163Z",
 *              "tripitLinked": true
 *            }
 *          ],
 *          "amenities": [
 *            {
 *              "id": 2,
 *              "tripId": 2,
 *              "name": "Moop Hotel",
 *              "type": "lodging",
 *              "point": {
 *                "lon": -21.915021,
 *                "lat": 64.140129
 *              }
 *            },
 *            {
 *              "id": 3,
 *              "tripId": 2,
 *              "name": "Gropber",
 *              "type": "restaurant",
 *              "point": {
 *                "lon": -21.949912,
 *                "lat": 64.078395
 *              }
 *            }
 *          ]
 *        }
 * 
 * @apiError (404) geocode_failed the string inputted by the user did not correspond to a real location.
 */
trip.get('/trip/nearby', apiWrap(async (req, res) => {
    const geocoded = await geocode.forward(req.query.human_location);
    if (!geocoded) {
        throw Boom.notFound('geocode_failed');
    }
    // TODO: don't hardcode limits
    const nearbyTrips = await tripModel.findNearby(geocoded, 5);
    const experts = await user.findExperts(geocoded);
    const uniqueUserIds = R.uniq(nearbyTrips.map(R.path(['userId'])));
    // TODO: don't hardcode this either
    const nearbyUsers = await pMap(uniqueUserIds, cUserId => user.findOneById(cUserId), { concurrency: 4 });
    const nearbyAmenities = R.flatten(await pMap(nearbyTrips, cTrip => amenity.findByTrip(cTrip), { concurrency: 4 }));

    const sanitizedExperts = experts.map(convert.user2SanitizedOther);
    const sanitizedNearbyUsers = nearbyUsers.map(convert.user2SanitizedOther);
    const sanitizedNearbyTrips = nearbyTrips.map(convert.trip2SanitizedOther);
    const sanitizedNearbyAmenities = nearbyAmenities.map(convert.amenity2SanitizedOther);
    const sanitizedGeocoded = convert.geocoded2Sanitized(geocoded);
    res.sendSuccess({
        geocoded: sanitizedGeocoded,
        trips: sanitizedNearbyTrips,
        experts: sanitizedExperts,
        users: sanitizedNearbyUsers,
        amenities: sanitizedNearbyAmenities,
    });
}));

module.exports = trip;
