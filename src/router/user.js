'use strict';

const R = require('ramda');
const express = require('express');
const Boom = require('boom');

const convert = require('../lib/convert');
const apiWrap = require('../lib/api-wrap');
const tripModel = require('../model/trip-db');
const userModel = require('../model/user');

const user = new express.Router();

/**
 * @api {get} /user/:id fetch a user's profile and trips
 * @apiName getUser
 * @apiGroup user
 * 
 * @apiParam {number} id the internal user ID. If 0, will fetch own user.
 * 
 * @apiSuccess {object} profile
 * @apiSuccess {number} profile.id the internal user ID
 * @apiSuccess {string{..255}} profile.givenName the user's first name
 * @apiSuccess {string{..255}} profile.familyName the user's given name
 * @apiSuccess {string{..255}} [profile.email] the user's email (only for your own user, id=0)
 * @apiSuccess {string{..255}} [profile.image] a url to the linkedin profile image
 * @apiSuccess {string{..255}} profile.headline a vey short headline for a user
 * @apiSuccess {string{..255}} profile.position a slightly longer description of the user
 * @apiSuccess {string{..255}} profile.humanLocation a human-readable string representing the person's location (for example, "Greater Seattle Area"
 * @apiSuccess {string} profile.createdAt when the user created their BizTraveler account (regardless of how old their linkedin account is). In some ISO string format, you can do new Date(this_string)
 * @apiSuccess {boolean} profile.tripitLinked whether the user has a linked tripit account
 *
 * @apiSuccess {object[]} trips
 * @apiSuccess {number} trip.id internal ID
 * @apiSuccess {number} trip.userId user ID related to the trip
 * @apiSuccess {string{..255}} trip.startedAt when the trip started
 * @apiSuccess {string{..255}} trip.endedAt when the trip ended
 * @apiSuccess {object} trip.visitedPoint a lat/long pair of the "main" trip location
 * @apiSuccess {string{..255}} trip.visitedName a human readable name of the "main" trip location
 * @apiSuccess {string} [trip.description] the description of the trip, set through the PATCH /trip/:id
 * @apiSuccess {string{..255}} [trip.image] a url to the image set by /trip/:id
 * 
 * @apiSuccessExample Simple user:
 *    {
 *      "status": "success",
 *      "profile": {
 *        "id": 1,
 *        "givenName": "Mark",
 *        "familyName": "Polyakov",
 *        "image": null,
 *        "headline": "hello",
 *        "position": "developer at BizTraveler",
 *        "humanLocation": "United States",
 *        "createdAt": "2018-09-26T02:42:01.163Z",
 *        "tripitLinked": true
 *      },
 *      "trips": [
 *        {
 *          "id": 2,
 *          "userId": 1,
 *          "startedAt": "2018-06-03T07:00:00.000Z",
 *          "endedAt": "2018-06-04T07:00:00.000Z",
 *          "visitedPoint": {
 *            "lon": -21.89541,
 *            "lat": 64.13548
 *          },
 *          "visitedName": "Reykjavik, Iceland",
 *          "description": "hello world.",
 *          "image": "https://i.imgur.com/example.png"
 *        }
 *      ]
 *    }
 * 
 * @apiError (404) user_not_found Invalid ID was passed
 */
user.get('/user/:id', apiWrap(async (req, res) => {
    const isSelf = +req.params.id === 0;
    const dirtyUser = isSelf ? req.user : await userModel.findOneById(+req.params.id);
    if (!dirtyUser) {
        throw Boom.notFound('user_not_found');
    }

    // fetch trips
    const dirtyTrips = await tripModel.findByUser(dirtyUser);

    const userSanitizer = isSelf ? convert.user2SanitizedSelf : convert.user2SanitizedOther;
    const tripSanitizer = isSelf ? convert.trip2SanitizedSelf : convert.trip2SanitizedOther;

    const sanitizedUser = userSanitizer(dirtyUser);
    const sanitizedTrips = dirtyTrips.map(tripSanitizer);

    res.sendSuccess({
        profile: sanitizedUser,
        trips: sanitizedTrips,
    });
}));

module.exports = user;
