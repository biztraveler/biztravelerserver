'use strict';

const express = require('express');
const passport = require('passport');
const assert = require('chai').assert;
const user = require('../model/user');
const tripit = require('../model/tripit-all');
const apiWrap = require('../lib/api-wrap');
const config = require('../lib/config');

const auth = new express.Router();

// GENERAL
/**
 * @api {get} /logout Logs out the user
 * @apiName logout
 * @apiGroup auth
 * 
 * @apiSuccess /callback-logout Redirect
 */
auth.get('/logout', (req, res) => {
    req.logout();
    res.redirect(`${config.frontendBaseUrl}/callback-logout`);
});

// LINKEDIN
/**
 * @api {get} /auth/linkedin/init Starts the linkedin authentication process
 * @apiName initLinkedin
 * @apiGroup auth
 * 
 * @apiSuccess /callback-linkedin-ok Redirect
 * @apiError /callback-linkedin-fail Redirect on failure. For example, user chose not to grant permissions.
 */
// the "state" is supposed to be unique each time, then verified in the callback. However, I think
// that's a pretty lame defense-in-depth mechanism, so I will ignore it.
auth.get('/linkedin/init', passport.authenticate('linkedin', { state: Math.random().toString() }));
auth.get('/linkedin/callback',
    // doing the weird shit because there are sometimes unexpected errors when
    // logging in. This will report them to the console
    passport.authenticate('linkedin', {
        successRedirect: `${config.frontendBaseUrl}/callback-linkedin-ok`,
        failureRedirect: `${config.frontendBaseUrl}/callback-linkedin-fail`,
    }),
);

// TRIPIT
/**
 * @api {get} /auth/tripit/init Starts the tripit authentication process
 * @apiName initTripit
 * @apiGroup auth
 * 
 * @apiSuccess /callback-tripit-ok Redirect
 * @apiError /callback-tripit-fail Redirect on failure, for example user chose not to grant permissions.
 * @apiError /callback-tripit-not-logged-in Redirect when the user is not even logged in via Linkedin yet. Users must login via linkedin before they can link their tripit account.
 */
auth.get('/tripit/init', passport.authorize('tripit'));
auth.get('/tripit/callback',
    passport.authorize('tripit', { failureRedirect: `${config.frontendBaseUrl}/callback-tripit-fail` }),
    apiWrap(async (req, res) => {
        assert.isObject(req.account);
        // TODO: make this better, use the existing middleware
        if (!req.user) {
            return res.redirect(`${config.frontendBaseUrl}/callback-tripit-not-logged-in`);
        }

        const tripitInfo = req.account;
        await user.linkTripit(req.user, tripitInfo.id, tripitInfo.token, tripitInfo.tokenSecret);
        // AHHHHHH
        const newFullUser = await user.findOneById(req.user.id);
        await tripit.updateTrips(newFullUser);

        res.redirect(`${config.frontendBaseUrl}/callback-tripit-ok`);
    }),
);

module.exports = auth;
