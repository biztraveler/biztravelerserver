// this file exports a knex instance set to our database, based on the knexfile.js config
'use strict';
const knex = require('knex');
const knexPostGis = require('knex-postgis');
const knexfile = require('../../knexfile');

const knexInstance = knex(knexfile[process.env.NODE_ENV]);
// installs itself as knexInstance.postgis
knexPostGis(knexInstance);

module.exports = knexInstance;

// check if postgres is running, warn the user if it is not. Maybe not the prettiest way to do this, but oh well
knexInstance.select(1)
.catch(() => console.error('PostgreSQL database is not running, expect problems...'));
