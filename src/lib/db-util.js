'use strict';

const db = require('./db');
const assert = require('chai').assert;

// everything in here returns snakeized (i.e, db format names)
const dbUtil = {
    /**
     * Upsert inserts a new row if none exists, or updates
     * an existing row based on the primary key. A user-specified
     * unique column is better than relying on any unique column
     * or automatically detecting the primary key because, lets say
     * multiple accounts have the same email, which should be unique,
     * but different linkedin IDs...using an "any unique column" system
     * (which is what PG's built in ON CONFLICT DO UPDATE clause does)
     * would cause the linkedin ID for an existing user to change
     * when logging in, while we would like it to throw an error.
     * @param table table name to upsert into
     * @param column the column to use to determine uniqueness
     * @param data what to upsert into that table
     * @return the inserted object snake mode, error if error
     */
    upsert: async (table, column, data) => {
        assert.typeOf(table, 'string');
        assert.typeOf(column, 'string');
        assert.isObject(data);
        assert.property(data, column);
 
        const columnValue = data[column];
        const countRes = await db(table).count('*').where(column, columnValue);
        const countRows = +countRes[0].count;
        if (countRows === 0) {
            const insertRes = await db(table).insert(data).returning('*');
            return insertRes[0];
        } else {
            const updateRes = await dbUtil.timestamp(db(table).update(data).where(column, columnValue).returning('*'));
            return updateRes[0];
        }
    },
    /**
     * @param table which table to select from
     * @param column the column to find on
     * @param value the value to find for this column
     * @return the full contents of that row, snakey, or null
     */
    findOne: async (table, column, value) => {
        assert.isString(table);
        assert.isString(column);
        assert.include(['string', 'number'], typeof value);

        const dbRes = await db(table).select('*').where(column, value);
        assert.isBelow(dbRes.length, 2);
        return dbRes.length === 1 ? dbRes[0] : null;
    },
    /**
     * Modifies a query to update updated_at column
     * @param query a partial knex update query
     * @return that query but will set updated_at
     */
    timestamp: query => {
        // it's hard to assert this well.
        assert.isObject(query);

        return query.update('updated_at', new Date());
    },
};
module.exports = dbUtil;
