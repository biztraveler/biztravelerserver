'use strict';

const debug = require('debug')('biz.main');
const express = require('express');
const expressSession = require('express-session');
const sessionFileStore = require('session-file-store')(expressSession);
const passport = require('passport');
const bodyParser = require('body-parser');

const config = require('./config');
const passportSetup = require('./passport-setup.js');

const apiMiddleware = require('../middleware/api');
const authRouter = require('../router/auth');
const userRouter = require('../router/user');
const tripRouter = require('../router/trip');

const app = express();

// EXPRESS CONFIGURATION
// possible things to put here: set "proxy" if we are behind a reverse proxy in production

// MIDDLEWARE
const fileStoreOpts = {
    path: config.sessionsPath,
    ttl: config.cookieMaxAge,
    // look, file systems aren't that unreliable.
    retries: 1,
    // just spouts random crap when file systems expires before the browser cookie
    logFn: () => null,
};
const sessionOpts = {
    store: new sessionFileStore(fileStoreOpts),
    secret: config.cookieSecret,
    resave: false,
    saveUninitialized: false,
    cookie: {
        domain: config.cookieDomain,
        maxAge: config.cookieMaxAge * 1000,
    },
};
app.use(expressSession(sessionOpts));
app.use(passport.initialize());
app.use(passport.session());
app.use(bodyParser.json());

// ROUTERS
app.use('/auth/', authRouter);
app.use('/api/v1/', apiMiddleware, userRouter, tripRouter);

// LISTEN
app.listen(config.port);
