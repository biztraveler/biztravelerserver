// This will set up passport with the given express app.
'use strict';

const assert = require('chai').assert;
const passport = require('passport');
const debug = require('debug')('biz.passport-setup');
const LinkedinStrategy = require('passport-linkedin-oauth2').Strategy;
const TripitStrategy = require('pirxpilot-passport-tripit').Strategy;
const config = require('./config');
const userModel = require('../model/user');

const linkedinOpts = {
    clientID: config.linkedinId,
    clientSecret: config.linkedinSecret,
    callbackURL: `${config.baseUrl}/auth/linkedin/callback`,
    scope: ['r_basicprofile', 'r_emailaddress'],
};
const linkedinVerify = async (token, tokenSecret, profile, done) => {
    /* 
     * The `token` contains an oauth token, but `tokenSecret` is empty.
     * Profile is a JSON object, i guess there's a convenience call to the
     * API in the passport module? The data will be like this:
     * { provider: 'linkedin',
        id: 'kCfKRA4w-D',
        displayName: 'Mark Polyakov',
        name: { familyName: 'Polyakov', givenName: 'Mark' },
        emails: [ { value: 'spamhereplease@markasoftware.com' } ],
      */

    // I don't know much about what is and isn't guaranteed in the profile,
    // so let's assert everything
    try {
        assert.match(profile.id, /[a-zA-Z0-9-_]{5,15}$/, 'user id');
        assert.match(profile.emails[0].value, /^[^@]+\@[^@]+\.[^@]+$/, 'email');
        assert.typeOf(profile.name.familyName, 'string', 'family name');
        assert.typeOf(profile.name.givenName, 'string', 'given name');
    } catch (e) {
        console.error(`ERRROR: Assertion failed in linkedinVerify: ${e}`);
        console.error('Profile:');
        console.error(profile);
        return done(e);
    }
    
    const rawProfile = profile._json;
    const linkedinUser = {
        linkedinId: profile.id,
        // I don't do `...profile.name` in case there are situations where
        // there are additional properties we don't want.
        familyName: profile.name.familyName,
        givenName: profile.name.givenName,
        email: profile.emails[0].value,
        image: rawProfile.pictureUrl || null,
        headline: rawProfile.headline,
        humanLocation: rawProfile.location.name,
    };
    // add the user position, if there is one
    const positions = rawProfile.positions;
    if (positions._total > 0) {
        // find which position is marked as curent
        const currentPosition = positions.values.find(c => c.isCurrent);
        const positionStr = `${currentPosition.title} at ${currentPosition.company.name}`;
        linkedinUser.position = positionStr;
    }
    const dbUser = await userModel.upsert(linkedinUser);
    return done(null, dbUser);
};
const linkedinStrategy = new LinkedinStrategy(linkedinOpts, linkedinVerify);

const tripitOpts = {
    consumerKey: config.tripitId,
    consumerSecret: config.tripitSecret,
    callbackURL: `${config.baseUrl}/auth/tripit/callback`,
};
const tripitVerify = (token, tokenSecret, profile, done) => {
    debug(`Tripit token: ${token} secret: ${tokenSecret}`);
    done(null, {
        id: profile.id,
        token,
        tokenSecret,
    });
};
const tripitStrategy = new TripitStrategy(tripitOpts, tripitVerify);

passport.use(linkedinStrategy);
passport.use(tripitStrategy);

passport.serializeUser((user, done) => done(null, user.id));
passport.deserializeUser(async (userId, done) => {
    const user = await(userModel.findOneById(userId));
    if (!user) {
        done('User does not exist', null);
    }
    assert.isNumber(user.id);
    return done(null, user);
});
