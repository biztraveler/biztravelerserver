'use strict';

// this is an implementation of the async/await error wrapper for express middleware like explained in this article: https://nemethgergely.com/error-handling-express-async-await/
// that article is a good read in general if you're going to do much serious work in Express.

const apiWrap = f => async (req, res, next) => {
    try {
        await f(req, res, next);
        next();
    } catch (e) {
        if (e.isBoom) {
            return res.sendFailure(e.output.statusCode, e.message);
        }
        next(e);
    }
};
module.exports = apiWrap;
