// this file exports global configuration options and provides default values.
'use strict';

const assert = require('chai').assert;
require('dotenv').config();

process.env.NODE_ENV = process.env.NODE_ENV || 'development';

const requiredOpts = [
    'BIZ_LINKEDIN_ID', 'BIZ_LINKEDIN_SECRET',
    'BIZ_TRIPIT_ID', 'BIZ_TRIPIT_SECRET',
];
const missingRequiredOpts = requiredOpts.filter(c => typeof process.env[c] === 'undefined');
if (missingRequiredOpts.length > 0) {
    throw new Error(`You are missing some required BizTraveler options! Set them in the .env file. Missing: ${missingRequiredOpts.join(', ')}`);
}

const config = {
    connectionString: process.env.BIZ_CONNECTION_STRING || 'postgresql://localhost/biztraveler',
    port: process.env.BIZ_HTTP_PORT || 8170,
    baseUrl: process.env.BIZ_BASE_URL || `http://localhost:8170`,
    frontendBaseUrl: process.env.BIZ_FRONTEND_BASE_URL || 'http://localhost:8080',
    cookieSecret: process.env.BIZ_COOKIE_SECRET || 'correct horse battery staple',
    cookieMaxAge: process.env.BIZ_COOKIE_MAX_AGE || 3000000,
    cookieDomain: process.env.BIZ_COOKIE_DOMAIN,
    sessionsPath: './.sessions',
    linkedinId: process.env.BIZ_LINKEDIN_ID,
    linkedinSecret: process.env.BIZ_LINKEDIN_SECRET,
    tripitId: process.env.BIZ_TRIPIT_ID,
    tripitSecret: process.env.BIZ_TRIPIT_SECRET,
    tripitApiBaseUrl: 'https://api.tripit.com/v1',
    nominatimSearchUrl: 'https://nominatim.openstreetmap.org/search/',
    nominatimUserAgent: process.env.BIZ_NOMINATIM_UA || 'BizTraveler (Development Mode) -- see bitbucket.org/biztraveler/biztravelerserver',
    maxTripsNearby: 100,
    maxDistanceTolerance: process.env.BIZ_MAX_DISTANCE_TOLERANCE || 7500,
    minExpertTrips: process.env.BIZ_MIN_EXPERT_TRIPS || 3,
};
module.exports = config;
