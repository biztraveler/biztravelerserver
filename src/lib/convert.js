'use strict';

const debug = require('debug')('biz.convert');
const assert = require('chai').assert;
const R = require('ramda');
const camelize = require('camelize');
const snakeize = require('snakeize');
const db = require('./db');

// converts from { lat: 52.8476278, lon: 82.927346 } -> a knex-postgis point
const coords2Db = latLong => db.postgis.setSRID(db.postgis.point(+latLong.lon, +latLong.lat), 4326);
// TODO: and the reverse
const db2Coords = geoJson => {
    // I can't believe it doesn't do this automatically.
    const geo = JSON.parse(geoJson);
    const [ lon, lat ] = geo.coordinates;
    return { lon, lat };
};
// these work on camelized trip objects
const overVisitedPoint = R.over(R.lensProp('visited_point'));
const overPoint = R.over(R.lensProp('point'));

const user2SanitizedTripit = user => R.assoc('tripitLinked', typeof user.tripitAccessKey === 'string', user);
const user2SanitizedSelf = R.pipe(
    user2SanitizedTripit,
    R.pick(['id', 'givenName', 'familyName', 'email', 'image', 'headline', 'position', 'humanLocation', 'createdAt', 'tripitLinked']),
);
const trip2SanitizedSelf = R.pick(
    ['id', 'userId', 'tripitId', 'startedAt', 'endedAt', 'visitedPoint', 'visitedName', 'description', 'image'],
);
const amenity2SanitizedSelf = R.pick(
    ['id', 'tripId', 'tripitId', 'name', 'type', 'point']
);

const convert = {
    tripit2Trip: (tripit, userId) => ({
        tripitId: tripit.id[0],
        startedAt: new Date(tripit.start_date[0]),
        endedAt: new Date(tripit.end_date[0]),
        visitedName: tripit.PrimaryLocationAddress[0].address[0],
        visitedPoint: {
            lat: tripit.PrimaryLocationAddress[0].latitude[0],
            lon: tripit.PrimaryLocationAddress[0].longitude[0],
        },
        userId,
    }),
    tripit2PublicTrips: (tripit, userId) => {
        const tripitTrips = tripit.Response.Trip;
        debug(`Number of trips (including private): ${tripitTrips.length}`);
        const convertedTrips = tripitTrips
            .filter(c => c.is_private[0] === 'false')
            .map(c => convert.tripit2Trip(c, userId));
        debug(`Number of trips (only public): ${convertedTrips.length}`);
        return convertedTrips;
    },

    // the Amenities returned by the following couple functions will be absent
    // of their biztraveler IDs.

    // tripIds is { tripitTripId: tripId }
    tripit2Amenity: tripit => ({
        tripitId: tripit.id[0],
        tripitTripId: tripit.trip_id[0],
        name: tripit.supplier_name[0],
        point: {
            lon: tripit.Address[0].longitude[0],
            lat: tripit.Address[0].latitude[0],
        },
    }),
    // tripIds should be { tripitId: tripId }
    tripit2Amenities: tripit => {
        // TODO: this probably shouldn't be here
        const amenityTypes = ['lodging', 'restaurant'];
        const tripitAmenities = R.flatten(amenityTypes.map(type => {
            debug(`Converting ${type} amenities.`);
            assert.isString(type);
            assert.isAbove(type.length, 1);

            const tripitKey = `${type[0].toUpperCase()}${type.slice(1)}Object`;
            debug(`Tripit key for this amenity type: ${tripitKey}`);
            const tripitValue = tripit.Response[tripitKey];
            if (!tripitValue) {
                debug('No suitable objects.');
                return [];
            }

            assert.isArray(tripitValue);
            debug(`Found ${tripitValue.length} amenities.`);
            const jsAmenities = tripitValue
                .map(convert.tripit2Amenity)
                .map(R.assoc('type', type));
            return jsAmenities;
        }));
        return tripitAmenities;
    },

    user2Db: snakeize,
    db2User: camelize,
    trip2Db: R.pipe(snakeize, overVisitedPoint(coords2Db)),
    db2Trip: R.pipe(overVisitedPoint(db2Coords), camelize),
    amenity2Db: R.pipe(R.omit(['tripitTripId']), snakeize, overPoint(coords2Db)),
    db2Amenity: R.pipe(camelize, overPoint(db2Coords)),

    // I'm just having fun now
    user2SanitizedSelf,
    user2SanitizedOther: R.pipe(
        user2SanitizedSelf,
        R.omit(['email']),
    ),

    trip2SanitizedSelf,
    trip2SanitizedOther: R.pipe(
        trip2SanitizedSelf,
        R.omit(['tripitId']),
    ),

    amenity2SanitizedSelf,
    amenity2SanitizedOther: R.pipe(
        amenity2SanitizedSelf,
        R.omit(['tripitId']),
    ),

    nominatim2Geocoded: nomnom => {
        assert.isArray(nomnom);
        const nom = nomnom[0];
        assert.isObject(nom);
        assert.isArray(nom.boundingbox);
        assert.isString(nom.display_name);
        assert.isObject(nom.geojson);
        assert.isString(nom.geojson.type);
        assert.isArray(nom.geojson.coordinates);
        return {
            center: {
                lat: nom.lat,
                lon: nom.lon,
            },
            viewport: {
                southwest: {
                    lat: nom.boundingbox[0],
                    lon: nom.boundingbox[2],
                },
                northeast: {
                    lat: nom.boundingbox[1],
                    lon: nom.boundingbox[3],
                },
            },
            geoJson: nom.geojson,
        };
    },

    geocoded2Sanitized: R.pick(['center', 'viewport']),
};
module.exports = convert;
